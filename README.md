<!-- BEGIN_TF_DOCS -->



## Resources

The following resources are used by this module:

- [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) (resource)
```yaml
locals {
  security_groups = [for security_group in var.resources : {
    name    = security_group.name
    vpc_id  = security_group.vpc_id
    tags    = try(security_group.tags != null ? security_group.tags : null, null)
    ingress = try(security_group.ingress != null ? security_group.ingress : [], [])
    egress  = try(security_group.egress != null ? security_group.egress : [], [])
  }]

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}


resource "aws_security_group" "this" {
  for_each    = merge({ for security_group in try(local.security_groups, []) : security_group.name => security_group })
  name        = each.value.name
  description = each.value.name
  vpc_id      = each.value.vpc_id

  dynamic "ingress" {
    for_each = try(each.value.ingress, [])
    content {
      from_port = ingress.value.from_port
      to_port   = ingress.value.to_port
      protocol  = -1
    }
  }

  dynamic "egress" {
    for_each = try(each.value.egress, [])
    content {
      from_port = egress.value.from_port
      to_port   = egress.value.to_port
      protocol  = -1
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }


  tags = merge(local.common_tags, each.value.tags != null ? each.value.tags : {
    Name        = each.value.name
    Description = "Created by ${var.project.createdBy}"
  })
}

```

## Providers

The following providers are used by this module:

- <a name="provider_aws"></a> [aws](#provider\_aws) (~>5.35.0)
```yaml
terraform {
  required_version = ">1.7.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }
}
```

## Required Inputs

The following input variables are required:

### <a name="input_project"></a> [project](#input\_project)

Description: some common settings like project name, environment, and tags for all resources-objects

Type:

```hcl
object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
```

### <a name="input_resources"></a> [resources](#input\_resources)

Description: resources of objects to create

Type:

```hcl
list(object({
    name   = string
    vpc_id = string
    tags   = optional(map(string))
    ingress = list(object({
      from_port : number
      to_port : number
    }))
    egress = optional(list(object({
      from_port : number
      to_port : number
    })))
  }))
```

## Optional Inputs

No optional inputs.
```yaml
variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = list(object({
    name   = string
    vpc_id = string
    tags   = optional(map(string))
    ingress = list(object({
      from_port : number
      to_port : number
    }))
    egress = optional(list(object({
      from_port : number
      to_port : number
    })))
  }))
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
```

## Outputs

The following outputs are exported:

### <a name="output_debug"></a> [debug](#output\_debug)

Description: For debug purpose.

### <a name="output_security_groups_arns"></a> [security\_groups\_arns](#output\_security\_groups\_arns)

Description: Arns of the created security\_groups.

### <a name="output_security_groups_ids"></a> [security\_groups\_ids](#output\_security\_groups\_ids)

Description: Ids of the created security\_groups.
```yaml
output "security_groups_ids" {
  description = "Ids of the created security_groups."
  value = [
    for security_group in aws_security_group.this : security_group.id
  ]
}

output "security_groups_arns" {
  description = "Arns of the created security_groups."
  value = [
    for security_group in aws_security_group.this : security_group.arn
  ]
}

output "debug" {
  description = "For debug purpose."
  value       = local.security_groups
}
```

## Example from YAML
```yaml
project:
  project: module_template
  region: eu-west-1
  profile: default
  createdBy: markitos
  environment: dev
  group: security_groups
security_groups:
  - name: example-security_group-yaml-1
    vpc_id: vpc_12345678
    tags:
      Name: example-security_group-yaml-1
    ingress:
      - from_port: 80
        to_port: 80
```

```yaml
provider "aws" {
  profile = local.manifest.project.profile
  region  = local.manifest.project.region
}

locals {
  manifest = yamldecode(file("${path.cwd}/manifest.yaml"))
}

module "module_usage_howto" {
  source    = "./../.."
  project   = local.manifest.project
  resources = local.manifest.security_groups
}
```

## Example from code .tf
```yaml
provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = [
    {
      name   = "security_group-1"
      vpc_id = "vpc-12345678"
      tags = {
        Name = "security_group-1"
      },
      ingress = [{
        from_port = 123
        to_port   = 456
      }]
    },
    {
      name   = "security_group-2"
      vpc_id = "vpc-12345678"
      ingress = [{
        from_port = 80
        to_port   = 81
      }]
      egress = [{
        from_port = 80
        to_port   = 81
      }]
    }
  ]

}
```
<!-- END_TF_DOCS -->