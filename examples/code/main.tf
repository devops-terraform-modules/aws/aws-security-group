provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = [
    {
      name   = "security_group-1"
      vpc_id = "vpc-12345678"
      tags = {
        Name = "security_group-1"
      },
      ingress = [{
        from_port = 123
        to_port   = 456
      }]
    },
    {
      name   = "security_group-2"
      vpc_id = "vpc-12345678"
      ingress = [{
        from_port = 80
        to_port   = 81
      }]
      egress = [{
        from_port = 80
        to_port   = 81
      }]
    }
  ]

}
