output "security_groups_ids" {
  description = "Ids of the created security_groups."
  value = [
    for security_group in aws_security_group.this : security_group.id
  ]
}

output "security_groups_arns" {
  description = "Arns of the created security_groups."
  value = [
    for security_group in aws_security_group.this : security_group.arn
  ]
}

output "debug" {
  description = "For debug purpose."
  value       = local.security_groups
}
