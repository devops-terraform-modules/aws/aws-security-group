locals {
  security_groups = [for security_group in var.resources : {
    name    = security_group.name
    vpc_id  = security_group.vpc_id
    tags    = try(security_group.tags != null ? security_group.tags : null, null)
    ingress = try(security_group.ingress != null ? security_group.ingress : [], [])
    egress  = try(security_group.egress != null ? security_group.egress : [], [])
  }]

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}


resource "aws_security_group" "this" {
  for_each    = merge({ for security_group in try(local.security_groups, []) : security_group.name => security_group })
  name        = each.value.name
  description = each.value.name
  vpc_id      = each.value.vpc_id

  dynamic "ingress" {
    for_each = try(each.value.ingress, [])
    content {
      from_port = ingress.value.from_port
      to_port   = ingress.value.to_port
      protocol  = -1
    }
  }

  dynamic "egress" {
    for_each = try(each.value.egress, [])
    content {
      from_port = egress.value.from_port
      to_port   = egress.value.to_port
      protocol  = -1
    }
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }


  tags = merge(local.common_tags, each.value.tags != null ? each.value.tags : {
    Name        = each.value.name
    Description = "Created by ${var.project.createdBy}"
  })
}

